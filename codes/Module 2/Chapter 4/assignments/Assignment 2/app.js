function dynamic() {
    var input = document.getElementById("input");
    var table = document.getElementById("table_1");
    var count = 1;
    var num = +input.value;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    for (count = 1; count <= num; count++) {
        var row = table.insertRow(); // to insert row
        var cell = row.insertCell(); //to insert cell
        var text = document.createElement("input"); //to enter text
        //for first column which will print given number
        text.type = "text";
        text.style.textAlign = "center";
        text.value = num.toString();
        cell.appendChild(text);
        //for second column which will print "*"
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = "*";
        cell.appendChild(text);
        //for the third column which will print the count 1 2 3 4....upto given number
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        //for the fourth coloumn to print "=" sign
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = "=";
        cell.appendChild(text);
        //for the fifth column to print the num*count as an answer
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (num * count).toString();
        cell.appendChild(text);
        //var cell:HTMLTableDataCellElement=row.insertCell();
    }
}
//# sourceMappingURL=app.js.map