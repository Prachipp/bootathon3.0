function area()
{

let ta1 : HTMLInputElement = <HTMLInputElement>document.getElementById("ta1");
let ta2 : HTMLInputElement = <HTMLInputElement>document.getElementById("ta2");
let tb1 : HTMLInputElement = <HTMLInputElement>document.getElementById("tb1");
let tb2 : HTMLInputElement = <HTMLInputElement>document.getElementById("tb2");
let tc1 : HTMLInputElement = <HTMLInputElement>document.getElementById("tc1");
let tc2 : HTMLInputElement = <HTMLInputElement>document.getElementById("tc2");
let ans : HTMLParagraphElement = <HTMLParagraphElement>document.getElementById("ans");

var x1:number= parseFloat(ta1.value);
var y1:number= parseFloat(ta2.value);
var x2:number= parseFloat(tb1.value);
var y2:number= parseFloat(tb2.value);
var x3:number= parseFloat(tc1.value);
var y3:number= parseFloat(tc2.value);

//calculate the area of main triangle
var ar:number=Math.abs((x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2);
ans.innerHTML="Answer-<br>The area is: "+ar.toString();

//now to check whether the point lies inside or outside
let tp1 : HTMLInputElement = <HTMLInputElement>document.getElementById("tp1");
let tp2 : HTMLInputElement = <HTMLInputElement>document.getElementById("tp2");

var x: number=parseFloat(tp1.value);
var y: number=parseFloat(tp2.value);

//calculate the areas of 3 small triangles using the given point coordinates
var p1:number=Math.abs((x*(y1-y2)+x1*(y2-y)+x2*(y-y1))/2);
var p2:number=Math.abs((x*(y2-y3)+x2*(y3-y)+x3*(y-y2))/2);
var p3:number=Math.abs((x*(y3-y1)+x3*(y1-y)+x1*(y-y3))/2);

var sum:number=p1+p2+p3;

//check the condition
if(Math.abs(ar-sum)<0.000001)
{
    ans.innerHTML+= "<br>The point lies inside the triangle";
}
else{
    ans.innerHTML+= "<br>The point lies outside the triangle";
}
}