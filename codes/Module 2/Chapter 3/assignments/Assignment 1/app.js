function area() {
    let ta1 = document.getElementById("ta1");
    let ta2 = document.getElementById("ta2");
    let tb1 = document.getElementById("tb1");
    let tb2 = document.getElementById("tb2");
    let tc1 = document.getElementById("tc1");
    let tc2 = document.getElementById("tc2");
    let ans = document.getElementById("ans");
    var x1 = parseFloat(ta1.value);
    var y1 = parseFloat(ta2.value);
    var x2 = parseFloat(tb1.value);
    var y2 = parseFloat(tb2.value);
    var x3 = parseFloat(tc1.value);
    var y3 = parseFloat(tc2.value);
    //calculate the area of main triangle
    var ar = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
    ans.innerHTML = "Answer-<br>The area is: " + ar.toString();
    //now to check whether the point lies inside or outside
    let tp1 = document.getElementById("tp1");
    let tp2 = document.getElementById("tp2");
    var x = parseFloat(tp1.value);
    var y = parseFloat(tp2.value);
    //calculate the areas of 3 small triangles using the given point coordinates
    var p1 = Math.abs((x * (y1 - y2) + x1 * (y2 - y) + x2 * (y - y1)) / 2);
    var p2 = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2);
    var p3 = Math.abs((x * (y3 - y1) + x3 * (y1 - y) + x1 * (y - y3)) / 2);
    var sum = p1 + p2 + p3;
    //check the condition
    if (Math.abs(ar - sum) < 0.000001) {
        ans.innerHTML += "<br>The point lies inside the triangle";
    }
    else {
        ans.innerHTML += "<br>The point lies outside the triangle";
    }
}
//# sourceMappingURL=app.js.map